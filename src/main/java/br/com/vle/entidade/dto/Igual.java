package br.com.vle.entidade.dto;

/**
 *
 * @author Victor Godoi
 */

public class Igual implements OperadorQuery {

	private String field;
	private Object value;
	
	
	
	public Igual(String field, Object value) {
		super();
		this.field = field;
		this.value = value;
	}

	public String getAtributo() {
		return field;
	}

	public Object getValor() {
		return value;
	}

	public void operacao(StringBuffer hql) {
		hql.append(field);
		hql.append(" = ");
		hql.append(" :");
		hql.append(field);

	}

}
