package br.com.vle.entidade.dto;

/**
 *
 * @author Victor Godoi
 */

public interface OperadorQuery {
	String getAtributo();
	Object getValor();
	void operacao(StringBuffer hql);
}
