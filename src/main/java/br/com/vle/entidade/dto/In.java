package br.com.vle.entidade.dto;

/**
 *
 * @author Victor Godoi
 */

public class In implements OperadorQuery {

	private String field;
	private Object value;
	private Boolean not;
	
	
	
	public In(String field, Object value) {
		super();
		this.field = field;
		this.value = value;
		this.not = Boolean.FALSE;
	}
	
	public In(String field, Object value, Boolean not) {
		super();
		this.field = field;
		this.value = value;
		this.not = not;
	}

	public String getAtributo() {
		return field;
	}

	public Object getValor() {
		return value;
	}

	public void operacao(StringBuffer hql) {
		hql.append(field);
		if(not){
			hql.append(" not in ");
		}else{
			hql.append(" in ");
		}
		hql.append(" in ");
		hql.append(" (:");
		hql.append(field);
		hql.append(")");

	}

}
