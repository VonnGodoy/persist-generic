package br.com.vle.entidade.dto;

/**
 *
 * @author Victor Godoi
 */

public class Like implements OperadorQuery {

	private String field;
	private Object value;
	
	
	
	public Like(String field, Object value) {
		this.field = field;
		this.value = value;
	}

	public String getAtributo() {
		return field;
	}

	public Object getValor() {
		return "%"+value.toString()+"%";
	}

	public void operacao(StringBuffer hql) {
		hql.append(field);
		hql.append(" like ");
		hql.append(" :");
		hql.append(field);

	}

}
