package br.com.vle.entidade;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

/**
 *
 * @author Victor Godoi
 */
@MappedSuperclass
public abstract class BaseEntity<T extends Serializable> implements Serializable {

    private static final long serialVersionUID = 1L;

    public abstract T getId();

    public abstract void setId(T id);

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @SuppressWarnings("rawtypes")
	@Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        } else if (!(obj instanceof BaseEntity)) {
            return false;
        } else if (((BaseEntity) obj).getId() !=null && ((BaseEntity) obj).getId().equals(this.getId())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "entity." + this.getClass() + "[ id=" + getId() + " ] ";
    }
}
