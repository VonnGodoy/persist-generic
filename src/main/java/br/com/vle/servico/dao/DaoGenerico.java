package br.com.vle.servico.dao;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.vle.entidade.BaseEntity;
import br.com.vle.entidade.dto.DTOFiltroBase;

/**
 *
 * @author Victor Godoi
 */

public abstract class DaoGenerico<T extends BaseEntity<?>> {

    @Inject
    private DaoUniversal daoUniversal;
    private Class<T> type;

    /**
     * Default constructor
     * 
     * @param type entity class
     */
    public DaoGenerico(Class<T> type) {
        this.type = type;
    }

    /**
     * Criar entidade
     * 
     * @param T Object
     * @return
     */
    public T create(T t) {
        return daoUniversal.create(t);
    }

    /**
     * Salvar ou atualizar entidade
     * 
     * @param e Object
     * @return
     */
    public T save(T e) {
        if (e.getId() != null) {
            e = update(e);
            return e;
        } else {
            create(e);
            return e;
        }
    }

    /**
     * Pesquisar entidade
     * 
     * @param T Object
     * @param id
     * @return
     */
    public T find(Object id) {
        return daoUniversal.find(this.type, id);
    }

    /**
     * Remover entidade
     * 
     * @param Object
     * @param id
     */
    public void delete(Object id) {
        this.daoUniversal.delete(this.type, ((BaseEntity<?>) id).getId());
    }

    /**
     * Remover entidades em lote
     * 
     * @param <T>
     * @param items
     * @return boolean
     */
    public boolean deleteItems(T[] items) {
        for (T item : items) {
            this.daoUniversal.delete(this.type, ((BaseEntity<?>) item).getId());
            this.daoUniversal.flush();
        }
        return true;
    }

    /**
     * Atualizar entidade
     * 
     * @param <T>
     * @param t
     * @return Objeto Atualizado
     */
    public T update(T item) {
        return (T) this.daoUniversal.update(item);
    }

    /**
     * Retorna uma lista de acordo com os criterios informados
     * 
     * @param namedQueryName
     * @return List
     */
    @SuppressWarnings("unchecked")
    public <W> List<W> buscaPorNamedQuery(String namedQueryName) {
        return this.daoUniversal.createNamedQuery(namedQueryName).getResultList();
    }

    /**
     * Retorna uma lista de acordo com os criterios informados
     * 
     * @param namedQueryName
     * @param parameters
     * @return List
     */
    @SuppressWarnings("unchecked")
    public <W> List<W> buscaPorNamedQuery(String namedQueryName, Map parameters) {
        return this.daoUniversal.buscaPorNamedQuery(namedQueryName, parameters);
    }

    /**
     * Retorna a quantidade de itens de de acordo com a solicitacao lazy loading / pagination
     * 
     * @param namedQueryName
     * @param start
     * @param end
     * @return List
     */
    @SuppressWarnings("unchecked")
    public <W> List<W> buscaPorNamedQuery(String namedQueryName, int start, int end) {
        return this.daoUniversal.buscaPorNamedQuery(namedQueryName, start, end);
    }

    public List<T> buscarPorFiltroDTO(String hql, DTOFiltroBase filtro) {
        return this.daoUniversal.buscarPorFiltroDTO(hql, null, filtro);
    }

    @SuppressWarnings("unchecked")
    public List<T> buscarPorFiltroDTO(String hql, String apelidoTabela, DTOFiltroBase filtro) {
        return this.daoUniversal.buscarPorFiltroDTO(hql, apelidoTabela, filtro);
    }

    public int totalPorFiltroDTO(String hql, DTOFiltroBase filtro) {
        return this.daoUniversal.totalPorFiltroDTO(hql, null, filtro);
    }

    public int totalPorFiltroDTO(String hql, String apelidoTabela, DTOFiltroBase filtro) {
        return this.daoUniversal.totalPorFiltroDTO(hql, apelidoTabela, filtro);
    }

    /**
     * Criador de Query
     * 
     * @param String
     * @return Query
     */
    public Query createQuery(String query) {
        return this.daoUniversal.createQuery(query);
    }

    public EntityManager getEm() {
        return this.daoUniversal.getEm();
    }

}